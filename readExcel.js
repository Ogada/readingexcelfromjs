const express = require('express');
const xlsx = require('xlsx');
const path = require('path');

const app = express();

// Serve static files from the 'public' directory
app.use(express.static(path.join(__dirname, 'public')));

// Define route to handle file reading and conversion
app.get('/data', (req, res) => {
    const dataPathExcel = "testExcel.xlsx";
    const wb = xlsx.readFile(dataPathExcel);
    const sheetName = wb.SheetNames[0];
    const sheetValue = wb.Sheets[sheetName];
    const excelData = xlsx.utils.sheet_to_json(sheetValue);
    res.json(excelData);
});

// Route to trigger the VBA macro
app.get('/triggerMacro', (req, res) => {
    try {
        // Load the workbook containing the VBA macro
        const workbookPath = path.join(__dirname, 'testExcel2.xlsm');
        const workbook = xlsx.readFile(workbookPath);

        // Access the worksheet
        const sheetIndex = 0; // Change this to the correct sheet index
        const sheetName = workbook.SheetNames[sheetIndex];
        const worksheet = workbook.Sheets[sheetName];

        if (!worksheet) {
            throw new Error(`Worksheet '${sheetName}' not found in the workbook.`);
        }

        // Modify the worksheet data as needed
        const newDataRow = { Sno: 5, Fruits: "Orange" };
        const newCellAddress = 'A5';
        worksheet[newCellAddress] = { v: newDataRow.Sno, t: 'n' };
        worksheet[`B5`] = { v: newDataRow.Fruits, t: 's' };

        // Save the modified workbook
        const outputPath = path.join(__dirname, 'modifiedExcel.xlsx');
        xlsx.writeFile(workbook, outputPath);

        // Read the modified Excel file and send the data as JSON
        const modifiedDataPathExcel = outputPath;
        const modifiedWb = xlsx.readFile(modifiedDataPathExcel);
        const modifiedSheetName = modifiedWb.SheetNames[0];
        const modifiedSheetValue = modifiedWb.Sheets[modifiedSheetName];
        const modifiedExcelData = xlsx.utils.sheet_to_json(modifiedSheetValue);
        res.json(modifiedExcelData);
    } catch (error) {
        console.error('Error triggering macro:', error);
        res.status(500).send('Internal Server Error');
    }
});

// Start the server
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
